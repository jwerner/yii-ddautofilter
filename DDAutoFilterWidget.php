<?php
/**
 * DDAutoFilterWidget Class File
 * 
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @link http://www.diggin-data.de
 */

/**
 * DDAutoFilterWidget creates an auto-filter popup to filter  a CGridView
 *
 * @author  Joachim Werner <joachim.werner@diggin-data.de>
 * @version 0.1
 */
class DDAutoFilterWidget extends CWidget
{
    public $gridId;

    // {{{ run
    /**
     * Runs the widget 
     * 
     * @access public
     * @return void
     */
    public function run()
    {
        $this->registerClientScripts();
        echo $this->createMarkup();
    } // }}} 
    // {{{ registerClientScripts
    /**
     * Registers the clientside widget files (css & js)
     */
    private function registerClientScripts() {
        // Get the resources path
        $resources = dirname(__FILE__).'/resources';

        $cs = Yii::app()->clientScript;
        // publish the files
        $baseUrl = Yii::app()->assetManager->publish($resources);
        // Stylesheet
        if(is_file($resources.'/styles.css')) {
            $cs->registerCssFile($baseUrl.'/styles.css');
        }
        // JavaScript
        if(is_file($resources.'/ddautofilter.js')) {
            $cs->registerScriptFile($baseUrl.'/ddautofilter.js');
        }
   } // }}}     
    // {{{ createMarkup
    /**
     * Creates the widget's markup 
     * 
     * @access public
     * @return void
     */
    public function createMarkup()
    {
        $this->render(
            'widget',
            array(
                'gridId'=>$this->gridId,
            )
        );
    } // }}} 
}
