<?php
/**
 * DDAutoFilterDataColumn class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */

/**
 * DDAutoFilterDataColumn renders the autofilter filter cell in the gridview
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 * @version 0.1
 */
class DDAutoFilterDataColumn extends TbDataColumn
{
    public $columnType='text';
    // {{{ renderFilterCell
    /**
     * Renders the filter cell.
     */
    public function renderFilterCell()
    {
        $filterValues = array();
        echo '<td nowrap="nowrap"><div class="filter-container">';

        // See zii/widgets/grid/CDataColumn.php:
        if(is_string($this->filter))
        {
            // DEBUG 
            echo 'string';
            echo $this->filter;
        }
        elseif($this->filter!==false && $this->grid->filter!==null && $this->name!==null && strpos($this->name,'.')===false)
        {
            echo CHtml::activeHiddenField($this->grid->filter, $this->name, array('id'=>false));
            if(is_array($this->filter)) {
                // DEBUG echo 'array';
                //echo CHtml::activeDropDownList($this->grid->filter, $this->name, $this->filter, array('id'=>false,'prompt'=>''));
                $filterValues = CJSON::encode($this->filter);
            } elseif($this->filter===null) {
                // DEBUG echo 'null';
                //echo $this->grid->filter->{$this->name};
            }
            echo $this->getFilterText($this->grid->filter->{$this->name});
        }
        else
            parent::renderFilterCellContent();

        if($this->name !== null) {
            if( $this->grid->filter->{$this->name}=='' )
                $icon = 'arrow-down';
            else
                $icon = 'filter';
            echo '&nbsp;<i class="icon-'.$icon.' autofilter" style="cursor:pointer" '
                .'data-column-type="'.$this->columnType.'" '
                .'data-grid="'.$this->grid->id.'" '
                .'data-model-name="'.get_class($this->grid->filter).'" '
                .'data-attribute="'.$this->name.'" '
                .'data-label="'.CHtml::encode($this->grid->dataProvider->model->getAttributeLabel($this->name)).'" ';
            if(count($filterValues)>0) 
                echo 'data-filter-values=\''.$filterValues.'\' ';
            
            $sort = $this->grid->dataProvider->getSort();
            if ($sort->resolveAttribute($this->name) !== false)
                echo 'data-sort="true" ';
            else
                echo 'data-sort="false" ';
            if($this->filter!==false && $this->grid->filter!==null && $this->name!==null && $this->grid->filter->{$this->name} !=='')
               echo 'data-value="'.base64_encode($this->grid->filter->{$this->name}).'" ';
            echo '></i>';
        }
        echo '</div></td>';
    } // }}} 
    // {{{ getFilterText
    /**
     * Prettify filter settings
     *
     * @return string
     */
    public function getFilterText($filter)
    {
        // Array of operators => replacement, prefix, postfix
        $operators = array(
            'EQUALS' => array('', '', ''),
            'NOT-EQUALS' => array('<>', '', ''),
            'GT' => array('>', '', ''),
            'GTE' => array('>=', '', ''),
            'LT' => array('<', '', ''),
            'LTE' => array('<=', '', ''),
            'STARTS-WITH' => array('', '', '*'),
            'ENDS-WITH' => array('', '*', ''),
            'CONTAINS' => array('', '*', '*'),
            'NOT-CONTAINS' => array('', '! *', '*'),
            'IN'=>array('', '(', ')'),
        );

        $result = array();
        if(preg_match('/\[\{(.*)\}\]/', $filter, $matches))
        {
            $filters = CJSON::decode($filter);
            foreach($filters as $n=>$filter2) {
                $operator = array_key_exists($filter2['operator'], $operators) ? $operators[$filter2['operator']][0] : $filter2['operator'];
                $result[$n][] = $operator;
                $prefix = array_key_exists($filter2['operator'], $operators) ? $operators[$filter2['operator']][1] : '';
                if(is_array($filter2['value']))
                    $result[$n][] = join(";",$filter2['value']);
                else
                    $result[$n][] = $filter2['value'];
                $postfix = array_key_exists($filter2['operator'], $operators) ? $operators[$filter2['operator']][2] : '';
                if(isset($filter2['join']))
                    $result[$n][] = str_replace( array('AND', 'OR'), array(' & ', ' | '), $filter2['join']);
                
                $result[$n] = join('', $result[$n]);
                $result[$n] = $prefix.$result[$n].$postfix;
            }
        } else {
            $result = array($filter);
        }
        $final = join(' ', $result);
        if(strlen($final)>10)
            $final = '<span title="'.$final.'">'.substr($final,0,10).'...'.'</span>';
        return $final;
    } // }}} 
}
