DDAutoFilter
============

DDAutoFilter is a [Yii][yii] extension to display auto-filters in a CGridView 
similar to MS Excel auto-filters.

It is currently working with [Yii-bootstrap][yii-bootstrap] based Yii applications.


Installation
------------

Download or clone from [bitbucket](https://bitbucket.org/jwerner/yii-ddautofilter) under `protected/extensions/diggindata`, so that you have a folder `protected/extensions/diggindata/ddautofilter`.

Setup
-----

### Model Behavior

In your model's `behaviors` method, add the _DDAutoFilterSearchBehavior_:

~~~
public function behaviors(){
    return array(
        ...
        'autoFilterSearchBehavior' => array(
            'class' => 'ext.diggindata.ddautofilter.DDAutoFilterSearchBehavior',
        ),
        ...
    );
}
~~~

### Model / _search_ Method

In your CActiveReord model's `search()` method, for each column that shall be displayed 
using auto-filters, change the `$criteria` line.

~~~
// Replace:
$criteria->compare('title',$this->title,true);
// ... with:
$criteria->mergeWith($this->textSearchCriteria('title', $this->title));
~~~

For _date_ columns, use:

~~~
$criteria->mergeWith($this->dateSearchCriteria('title', $this->title));
~~~

For _timestamp_ columns, use:

~~~
$criteria->mergeWith($this->dateSearchCriteria('title', $this->title, 'U'));
~~~

#### Grid View

##### Grid View Properties

Add some code to the CGridView `afterAjaxUpdate` property:

~~~
<?php $this->widget('ext.bootstrap.widgets.TbGridView', array(
    'id'=>'your-model-grid', // see below
    ...
    'afterAjaxUpdate'=>'function(){jQuery("i.autofilter").click(showAutofilterDialog)}',
    ...
    )); ?>
~~~

##### Grid View Columns

~~~
    'columns' => array(
        ...
        array(
            'name'=>'level',
            'class'=>'ext.diggindata.ddautofilter.DDAutoFilterDataColumn',
            'columnType'=>'number', // or 'text' or 'date' or 'bool'
            'filter => array(1=>1, 2=>2, 3=>3), // will be displayed in values select multiple control
        ),
        ...
~~~

##### Auto-Filter Widget

After the CGridView widget code, add:

~~~
<?php $this->widget('ext.diggindata.ddautofilter.DDAutoFilterWidget', array('gridId' => 'your-model-grid')); ?>
~~~

## History

* **V0.1:**  (2013-05-06) Initial version

## Donate?

If you like this extension, please consider donating a bit:

[Donate with PayPal][paypal]


[paypal]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DDZUYNDEJ6XDY
[yii]: http://yiiframework.com/extensions/
[yii-bootstrap]: http://www.yiiframework.com/extension/bootstrap
