<!-- Dialog I -->
<div class="dialogtest" style=display:none><!-- {{{ -->
<form style="font-size:1em;" id="<?php echo $gridId; ?>-autofilter">
<input type="hidden" name="ajax" value="site-page-grid" />
<a href="#" name="sort1" class="autofilter-sort asc" value="" onclick="doSort('asc');" /><i class="icon-arrow-down"></i><?php echo CHtml::encode(_('Ascending (A-Z)')); ?></a><br />
<a href="#" name="sort2" class="autofilter-sort desc" value=".desc"  onclick="doSort('desc');" /><i class="icon-arrow-up"></i><?php echo CHtml::encode(_('Descending (Z-A)')) ?></a><hr />
<span class="clear-filter"><i class="icon-remove"></i> Remove Filter (<span class="attributeLabel"></span>)</span><hr />
<span class="filters-more">Textfilter</span><br />
<div class="moreFilters" style="display:none;max-height:100px;overflow:auto;">
<ul>

<li><a href="#" class="udf-equals"><?php echo CHtml::encode(_('Equals...')) ?></a></li>
<li><a href="#" class="udf-not-equals"><?php echo CHtml::encode(_('Not Equals...')) ?></a></li>

<li class="textfilters"><a href="#" class="udf-starts-with"><?php echo CHtml::encode(_('Starts with...')); ?></a></li>
<li class="textfilters"><a href="#" class="udf-ends-with"><?php echo CHtml::encode(_('Ends with...')); ?></a></li>

<li class="textfilters"><a href="#" class="udf-contains"><?php echo CHtml::encode(_('Contains...')) ?></a></li>
<li class="textfilters"><a href="#" class="udf-not-contains"><?php echo CHtml::encode(_("Doesn't contain...")) ?></a></li>

<li class="numberfilters"><a href="#" class="udf-gt"><?php echo CHtml::encode(_('Greater than...')) ?></a></li>
<li class="numberfilters"><a href="#" class="udf-gte"><?php echo CHtml::encode(_('Greater or equal...')) ?></a></li>
<li class="numberfilters"><a href="#" class="udf-lt"><?php echo CHtml::encode(_('Lower than...')) ?></a></li>
<li class="numberfilters"><a href="#" class="udf-lte"><?php echo CHtml::encode(_('Lower or equal...')) ?></a></li>
<li class="numberfilters"><a href="#" class="udf-between"><?php echo CHtml::encode(_('Between...')) ?></a></li>

<li class="boolfilters"><a href="#" class="udf-bool-yes"><?php echo CHtml::encode(_('Yes')) ?></a></li>
<li class="boolfilters"><a href="#" class="udf-bool-no"><?php echo CHtml::encode(_('No')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-between"><?php echo CHtml::encode(_('Between...')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-before"><?php echo CHtml::encode(_('Before...')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-after"><?php echo CHtml::encode(_('After...')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-date-day-next"><?php echo CHtml::encode(_('Tomorrow')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-day-this"><?php echo CHtml::encode(_('Today')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-day-last"><?php echo CHtml::encode(_('Yesterday')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-date-week-next"><?php echo CHtml::encode(_('Next Week')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-week-this"><?php echo CHtml::encode(_('This Week')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-week-last"><?php echo CHtml::encode(_('Last Week')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-date-month-next"><?php echo CHtml::encode(_('Next Month')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-month-this"><?php echo CHtml::encode(_('This Month')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-month-last"><?php echo CHtml::encode(_('Last Month')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-date-quarter-next"><?php echo CHtml::encode(_('Next Quarter')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-quarter-this"><?php echo CHtml::encode(_('This Quarter')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-quarter-last"><?php echo CHtml::encode(_('Last Quarter')) ?></a></li>

<li class="datefilters"><a href="#" class="udf-date-year-next"><?php echo CHtml::encode(_('Next Year')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-year-this"><?php echo CHtml::encode(_('This Year')) ?></a></li>
<li class="datefilters"><a href="#" class="udf-date-year-last"><?php echo CHtml::encode(_('Last Year')) ?></a></li>

<li><a href="#" class="udf-default"><?php echo CHtml::encode(_('User-defined Filter...')) ?></a></li>
</ul>
</div>
<hr />
<input type="text" name="textfilter" class="textfilter" />

<select name="values" multiple="multiple" style="display:none;">
<option value="__ALL__"><?php echo CHtml::encode(_('All')); ?></option>
</select>

</form>
</div><!-- }}} -->

<!-- Dialog II -->
<div class="userDefinedFilter" style="display:none"><!-- {{{ -->
<span class="attributeLabel"></span><br />
<select name="operator1"><!-- {{{ -->
    <option value="EQUALS"><?php echo CHtml::encode(_('Equals')) ?></option>
    <option value="NOT-EQUALS"><?php echo CHtml::encode(_("Doesn't equal")) ?></option>
    <option value="GT"><?php echo CHtml::encode(_('Greater than')) ?></option>
    <option value="GTE"><?php echo CHtml::encode(_('Greater or equal')) ?></option>
    <option value="LT"><?php echo CHtml::encode(_('Lower than')) ?></option>
    <option value="LTE"><?php echo CHtml::encode(_('Lower or equal')) ?></option>
    <option value="STARTS-WITH"><?php echo CHtml::encode(_('Starts with')) ?></option>
    <option value="ENDS-WITH"><?php echo CHtml::encode(_('Ends with')) ?></option>
    <option value="CONTAINS"><?php echo CHtml::encode(_('Contains')) ?></option>
    <option value="NOT-CONTAINS"><?php echo CHtml::encode(_("Doesn't contain")) ?></option>
</select>&nbsp;<!-- }}} -->
<input type="text" name="udftext1" value="" /><br />
<input type="radio" name="andOr" value="AND" />&nbsp;<?php echo CHtml::encode(_('And')) ?>&nbsp;
<input type="radio" name="andOr" value="OR" />&nbsp;<?php echo CHtml::encode(_('Or')) ?>&nbsp;
<select name="operator2"><!-- {{{ -->
    <option value="EQUALS"><?php echo CHtml::encode(_('Equals')) ?></option>
    <option value="NOT-EQUALS"><?php echo CHtml::encode(_("Doesn't equal")) ?></option>
    <option value="GT"><?php echo CHtml::encode(_('Greater than')) ?></option>
    <option value="GTE"><?php echo CHtml::encode(_('Greater or equal')) ?></option>
    <option value="LT"><?php echo CHtml::encode(_('Lower than')) ?></option>
    <option value="LTE"><?php echo CHtml::encode(_('Lower or equal')) ?></option>
    <option value="STARTS-WITH"><?php echo CHtml::encode(_('Starts with')) ?></option>
    <option value="ENDS-WITH"><?php echo CHtml::encode(_('Ends with')) ?></option>
    <option value="CONTAINS"><?php echo CHtml::encode(_('Contains')) ?></option>
    <option value="NOT-CONTAINS"><?php echo CHtml::encode(_("Doesn't contain")) ?></option>
</select><!-- }}} -->
<input type="text" name="udftext2" value="" /><br />
</div><!-- }}} -->

<?php Yii::app()->clientScript->registerScript('autofilterClickHandler', "
    //jQuery('i.autofilter').on('click',function(event) { showAutofilterDialog();});
    jQuery('i.autofilter').on('click',showAutofilterDialog);
    jQuery('.filters-more').click(function(e){
        e.preventDefault();
        if(moreFiltersShown==false) {
            jQuery('.moreFilters').show(400);
            moreFiltersShown = true;
        } else {
            jQuery('.moreFilters').hide(400);
            moreFiltersShown = false;
        }
    });
"); ?>
<!-- vim: set fdm=marker: -->
