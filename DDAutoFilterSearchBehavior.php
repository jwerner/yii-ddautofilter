<?php
/**
 * DDAutoFilterSearchBehavior class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
Yii::import('ext.diggindata.ddautofilter.DDDateHelper');

/**
 * This model behavior builds search conditions for the DDAutoFilter widget
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 * @version 0.1
 */
class DDAutoFilterSearchBehavior extends CActiveRecordBehavior
{
    // {{{ dateSearchCriteria
    /*
     * Date range search criteria
     * public $attribute name of the date attribute
     * public $value value of the date attribute
     * @return instance of CDbCriteria for the model's search() method
     */
    public function dateSearchCriteria($attribute, $value, $dateFormat='Y-m-d')
    {
        // Create a new db criteria instance
        $criteria = new CDbCriteria;

        // DEBUG var_dump($attribute, $value);
        if(is_null($value) or trim($value)=='')
            return $criteria;
        if(substr($value,0,1)=='[' and substr($value,-1)==']') {
        } else {
            $value = '[{"operator": "EQUALS", "value": "'.$value.'"}]';
            $this->getOwner()->$attribute = $value;
        }
        $filters = CJSON::decode($value);
        // DEBUG var_dump($filters);

        foreach($filters as $i=>$filter) 
        { 
            $op = '';
            $join = isset($filter['join']) ? $filter['join'] : 'AND';
            switch(trim($filter['operator']))
            {
                case 'EQUALS':
                case 'NOT-EQUALS':
                case 'GT':
                case 'GTE':
                case 'LT':
                case 'LTE':
                case 'STARTS-WITH':
                case 'ENDS-WITH':
                case 'CONTAINS':
                case 'NOT-CONTAINS':
                    if(preg_match('/^(?:\s*(<>|<=|>=|<|>|=))?(.*)$/',$filter['value'], $matches)) {
                        $filter['value'] = date("Y-m-d", CDateTimeParser::parse( $matches[2], CLocale::getInstance(Yii::app()->language)->getDateFormat('medium') ));
                        $op = $matches[1];
                    }
                    break;
            }
            switch(trim($filter['operator']))
            {
                // {{{ Date Ranges
                // {{{ Day
                // {{{ Yesterday
                case '_DAY_LAST_':
                    $criteria->compare($attribute,date($dateFormat, DDDateHelper::DayYesterday()));
                    break;
                // }}}
                // {{{ Today
                case '_DAY_THIS_':
                    $criteria->compare($attribute, date($dateFormat));
                    break;
                // }}}
                // {{{ Tomorrow
                case '_DAY_NEXT_':
                    $criteria->compare($attribute, date($dateFormat, DDDateHelper::DayTomorrow()));
                    break;
                // }}}
                // }}}
                // {{{ Week
                // {{{ Last Week
                case '_WEEK_LAST_':
                    $fromTo = DDDateHelper::WeekRelative(null,-1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ This Week
                case '_WEEK_THIS_':
                    $fromTo = DDDateHelper::WeekThis();
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ Next Week
                case '_WEEK_NEXT_':
                    $fromTo = DDDateHelper::WeekRelative(null,1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // }}}
                // {{{ Month
                // {{{ Last Month
                case '_MONTH_LAST_':
                    $fromTo = DDDateHelper::MonthRelative(null,-1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ This Month
                case '_MONTH_THIS_':
                    $fromTo = DDDateHelper::MonthThis();
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ Next Month
                case '_MONTH_NEXT_':
                    $fromTo = DDDateHelper::MonthRelative(null,1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // }}}
                // {{{ Quarter
                // {{{ Last Quarter
                case '_QUARTER_LAST_':
                    $fromTo = DDDateHelper::QuarterRelative(null,-1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ This Quarter
                case '_QUARTER_THIS_':
                    $fromTo = DDDateHelper::QuarterThis();
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ Next Quarter
                case '_QUARTER_NEXT_':
                    $fromTo = DDDateHelper::QuarterRelative(null,1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // }}}
                // {{{ Year
                // {{{ Last Year
                case '_YEAR_LAST_':
                    $fromTo = DDDateHelper::YearRelative(null,-1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ This Year
                case '_YEAR_THIS_':
                    $fromTo = DDDateHelper::YearThis();
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ Next Year
                case '_YEAR_NEXT_':
                    $fromTo = DDDateHelper::YearRelative(null,1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}
                // {{{ Year Todate
                case '_YEAR_TODATE_':
                    $fromTo = DDDateHelper::YearToDate(null,1);
                    $criteria->addCondition(
                        sprintf(
                            "t.%s BETWEEN '%s' AND '%s'",
                            $attribute,
                            date($dateFormat, $fromTo[0]),
                            date($dateFormat, $fromTo[1])
                        )
                    );
                    break;
                // }}}                // }}}
                // }}} 
                case 'EQUALS':
                    $criteria->compare($attribute, $op.$value, false, $join );
                    //$this->owner->$attribute = $value;
                    break;
                case 'NOT-EQUALS':
                    $criteria->addCondition("$attribute<>'{$filter['value']}'", $join);
                    break;
                case 'GT':
                    $criteria->addCondition("$attribute>'{$filter['value']}'", $join);
                    break;
                case 'GTE':
                    $criteria->addCondition("$attribute>='{$filter['value']}'", $join);
                    break;
                case 'LT':
                    $criteria->addCondition("$attribute<'{$filter['value']}'", $join);
                    break;
                case 'LTE':
                    $criteria->addCondition("$attribute<='{$filter['value']}'", $join);
                    break;
                case 'STARTS-WITH':
                    $criteria->addCondition("$attribute LIKE '{$filter['value']}%'", $join);
                    break;
                case 'ENDS-WITH':
                    $criteria->addCondition("$attribute LIKE '%{$filter['value']}'", $join);
                    break;
                case 'CONTAINS':
                    $criteria->addCondition("$attribute LIKE '%{$filter['value']}%'", $join);
                    break;
                case 'NOT-CONTAINS':
                    $criteria->addCondition("$attribute NOT LIKE '%{$filter['value']}%'", $join);
                    break;
            } 
        }
        // Return the search criteria to merge with the model's search() method
        return $criteria;
    } // }}} 
    // {{{ textSearchCriteria
    public function textSearchCriteria($attribute, $value)
    {
        // Create a new db criteria instance
        $criteria = new CDbCriteria;

        // DEBUG var_dump($attribute, $value);
        if(is_null($value) or trim($value)=='')
            return $criteria;
        if(substr($value,0,1)=='[' and substr($value,-1)==']') {
        } else {
            $value = '[{"operator": "EQUALS", "value": "'.$value.'"}]';
            $this->getOwner()->$attribute = $value;
        }
        $filters = CJSON::decode($value);
        // DEBUG var_dump($filters);
        foreach($filters as $i=>$filter) 
        {
            $join = isset($filter['join']) ? $filter['join'] : 'AND';
            // DEBUG var_dump($filter, $filter['value']);

            switch($filter['operator'])
            {
                case 'EQUALS':
                    $criteria->compare($attribute, $filter['value'], false, $join );
                    break;
                case 'NOT-EQUALS':
                    $criteria->addCondition("$attribute<>'{$filter['value']}'", $join);
                    break;
                case 'GT':
                    $criteria->addCondition("$attribute>'{$filter['value']}'", $join);
                    break;
                case 'GTE':
                    $criteria->addCondition("$attribute>='{$filter['value']}'", $join);
                    break;
                case 'LT':
                    $criteria->addCondition("$attribute<'{$filter['value']}'", $join);
                    break;
                case 'LTE':
                    $criteria->addCondition("$attribute<='{$filter['value']}'", $join);
                    break;
                case 'STARTS-WITH':
                    $criteria->addCondition("$attribute LIKE '{$filter['value']}%'", $join);
                    break;
                case 'ENDS-WITH':
                    $criteria->addCondition("$attribute LIKE '%{$filter['value']}'", $join);
                    break;
                case 'CONTAINS':
                    $criteria->addCondition("$attribute LIKE '%{$filter['value']}%'", $join);
                    break;
                case 'NOT-CONTAINS':
                    $criteria->addCondition("$attribute NOT LIKE '%{$filter['value']}%'", $join);
                    break;
                case 'BETWEEN':
                    list($valueStart, $valueEnd) = preg_split('/[\s]+AND|OR[\s]+/', $filter['value'], -1, PREG_SPLIT_NO_EMPTY);
                    $criteria->addBetweenCondition($attribute, $valueStart, $valueEnd, $join);
                    break;
                case 'IN':
                    $criteria->addInCondition($attribute, $filter['value'], $join);
                    break;
            }
        }
        // DEBUG var_dump($criteria);
        return $criteria;
    } // }}} 
}
