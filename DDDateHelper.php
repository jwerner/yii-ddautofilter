<?php
/*
 * DDDateHelper class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 

/**
 * Some date functions
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */

class DDDateHelper
{
// {{{ *** Date Methods ***
// {{{ Days
// {{{ DayTomorrow
function DayTomorrow($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    return strtotime("+1 day", $ts);
}
// }}} 
// {{{ DayYesterday
function DayYesterday($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    return strtotime("-1 day", $ts);
} // }}} 
// {{{ DaysRange
function DaysRange($start, $end, $workingDaysOnly=false)
{
    list($year,$month,$day) = explode('-',$start);
    $start = mktime(0,0,0,$month,$day,$year);
    list($year,$month,$day) = explode('-',$end);
    $end = mktime(23,59,59,$month,$day,$year);
    $tmp = array();
    $current = $start;
    while($current<=$end) {
        if($workingDaysOnly==true and in_array(date("w",$current),array(1,2,3,4,5)))
            $tmp[] = strtotime(date("Y-m-d",$current));
        $current = $current + +60*60*24;
    }
    return $tmp;
} // }}} 
// }}} 
// {{{ Weeks
function WeekFirstDay($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    $day = date("w",$ts);
    if($day==1) 
        return strtotime(date("Y-m-d",$ts));
    else
        return strtotime(date("Y-m-d",strtotime("last monday",$ts)));
}
function WeekLastDay($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    $day = date("w",$ts);
    if($day==6) 
        return strtotime(date("Y-m-d",$ts));
    else
        return strtotime(date("Y-m-d",strtotime("sunday",$ts)));
}
function WeekThis($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    return array(self::WeekFirstDay($ts),self::WeekLastDay($ts));
}
function WeekRelative($ts=null,$relative=0)
{
    if($ts==null) 
        $ts=mktime();
    $tsDiff = $relative*86400*7;
    return self::WeekThis($ts+$tsDiff);
}
// }}} 
// {{{ Months
function MonthFirstDay($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    if(date("j",$ts)==1 )
        return strtotime(date("Y-m-d",$ts));
    else
        return strtotime(date("Y-m-d",mktime(0,0,0,date("n",$ts),1,date("Y",$ts))));    
}
function MonthLastDay($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    $nextMonth = mktime(0,0,0,date("n",$ts)+1,1,date("Y",$ts));
    return strtotime(date("Y-m-d", strtotime("-1 day", $nextMonth)));
}
function MonthThis($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    return array(self::MonthFirstDay($ts),self::MonthLastDay($ts));
}
function MonthRelative($ts=null,$relative=0)
{
    if($ts==null) 
        $ts=mktime();
    $tsNew = mktime(0, 0, 0, date("n",$ts)+$relative,1,date("Y",$ts));
    return self::MonthThis($tsNew);
}
// }}} 
// {{{ Quarters
function QuarterFirstDay($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    $month = date("n", $ts);
    if($month>=1 and $month<=3)
        return strtotime(date("Y-m-d", mktime(0,0,0,1,1,date("Y",$ts))));
    elseif($month>=4 and $month<=6)
        return strtotime(date("Y-m-d", mktime(0,0,0,4,1,date("Y",$ts))));
    elseif($month>=7 and $month<=9)
        return strtotime(date("Y-m-d", mktime(0,0,0,7,1,date("Y",$ts))));
    else
        return strtotime(date("Y-m-d", mktime(0,0,0,10,1,date("Y",$ts))));
}
function QuarterLastDay($ts=null)
{
    $month = date("n", $ts);
    if($month>=1 and $month<=3)
        return strtotime(date("Y-m-d", mktime(0,0,0,3,31,date("Y",$ts))));
    elseif($month>=4 and $month<=6)
        return strtotime(date("Y-m-d", mktime(0,0,0,6,30,date("Y",$ts))));
    elseif($month>=7 and $month<=9)
        return strtotime(date("Y-m-d", mktime(0,0,0,9,30,date("Y",$ts))));
    else
        return strtotime(date("Y-m-d", mktime(0,0,0,12,31,date("Y",$ts))));
}
function QuarterThis($ts=null)
{
    if($ts==null) 
        $ts=mktime();
    return array(self::QuarterFirstDay($ts),self::QuarterLastDay($ts));
}
function QuarterRelative($ts=null,$relative=0)
{
    if($ts==null) 
        $ts=mktime();
    $month = date("n", $ts)+$relative*3;
    return self::QuarterThis(mktime(0,0,0,$month,1,date("Y",$ts)));
}
// }}} 
// {{{ Years
function YearFirstDay($ts=null)
{
    if($ts==null)
        $ts=mktime();
    return strtotime(date("Y-m-d",mktime(0,0,0,1,1,date("Y",$ts))));
}
function YearLastDay($ts=null)
{
    if($ts==null)
        $ts=mktime();
    return strtotime(date("Y-m-d H:i:s",mktime(23,59,59,12,31,date("Y",$ts))));
}
function YearThis($ts=null)
{
    if($ts==null)
        $ts=mktime();
    return array(self::YearFirstDay($ts),self::YearLastDay($ts));
}
function YearToDate($ts=null)
{
    if($ts==null)
        $ts=mktime();
    return array(strtotime(date("Y-m-d H:i:s", mktime(0,0,0,1,1,date("Y")))),strtotime(date("Y-m-d H:i:s",$ts)));
}
function YearRelative($ts=null,$relative=0)
{
    if($ts==null)
        $ts=mktime();
    $tsNew = mktime(0,0,0,date("n",$ts),date("j",$ts),date("Y",$ts)+$relative);
    return self::YearThis($tsNew);
}
// }}} 
// }}} 

}

/* vim: set ai expandtab tabstop=4 shiftwidth=4 softtabstop=4 fdm=marker fdc=4: */ 
?>
