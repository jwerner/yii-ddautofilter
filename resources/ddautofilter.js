/**
 * DDAUtofilter JavaScript
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
var grid;
var columnType;
var modelName;
var attribute;
var label;
var value;
var sort;
var filterValues;
var moreFiltersShown=false;
var filterCellName;
var sorting = false;
function showAutofilterDialog() // {{{ 
{
    var valueObj={};
    grid = $(this).attr('data-grid');
    columnType = $(this).attr('data-column-type');
    modelName = $(this).attr('data-model-name');
    attribute = $(this).attr('data-attribute');
    label = $(this).attr('data-label');
    value = $(this).attr('data-value');
    if(typeof value=='undefined') 
        value = '';
    if(value.replace(/^\s+|\s+$/g, '')!='')
        value = decode64($(this).attr('data-value'));
    // DEBUG alert('>'+value+'<');
    // DEBUG alert(typeof value);
    var suche = /^\[\{(.*)\}\]$/;
    if(value.match(suche)) {
        valueObj = JSON.parse(value);
    } else if(value!=='') {
        value = '[{"operator":"EQUALS","value":"'+value+'"}]';
        valueObj = JSON.parse(value);
    }

    console.log(valueObj);
    filterValues = $(this).attr('data-filter-values');
    if(typeof filterValues=='undefined') 
        filterValues = '';
    if(filterValues.replace(/^\s+|\s+$/g, '')!='')
        filterValues = JSON.parse(filterValues);
    else
        filterValues = {};
    // DEBUG alert(filterValues);
    // DEBUG alert(attribute);
    // DEBUG alert('>'+value+'<');

    // Add select values
    $('select[name=values]')
        .find('option[value!=__ALL__]')
        .remove();
    if(filterValues!={}) { 
        $('select[name=values]').show();
    }

    $.each(filterValues, function(key, value) {   
         $('select[name=values]')
            .append($('<option>', { value : key })
            .text(value)); 
    });

    var form = jQuery('#'+grid+'-autofilter');
    var textfilter = form.find('.textfilter');
    if(value.replace(/^\s+|\s+$/g, '')=='')
        textfilter.val('');
    else if(valueObj.length==1 && valueObj[0].operator=='EQUALS')
        textfilter.val(valueObj[0].value);
    else
        textfilter.val(value);
    
    if(valueObj[0]) {
        if(valueObj[0].operator=='IN') {
            $('select[name=values]').val(valueObj[0].value);
            $('.textfilter').val('');
        }
    }
    
    console.log(filterValues);
    sort = $(this).attr('data-sort');

    var posLeft=$(this).position().left+10;
    var posTop=$(this).position().top-50;
    
    var sort1 = form.find('input[name=sort1]');
    sort1.attr('name',modelName+'_sort');
    
    var sort2 = form.find('input[name=sort2]');
    sort2.attr('name',modelName+'_sort');
    
    // Rename textfield?
    // textfilter.attr('name', modelName+'['+attribute+']');
    filterCellName = modelName+'\\['+attribute+'\\]';


    jQuery('.attributeLabel').html(label);

    jQuery('.textfilters, .numberfilters, .boolfilters, .datefilters').hide();
    if(columnType=='text') {
        jQuery('.filters-more').html('Text Filters...');
        jQuery('.textfilters').show();
    } else if(columnType=='number') {
        jQuery('.filters-more').html('Number Filters...');
        jQuery('.numberfilters').show();
    } else if(columnType=='boolean') {
        jQuery('.filters-more').html('Yes/No Filters...');
        jQuery('.boolfilters').show();
    } else if(columnType=='date') {
        jQuery('.filters-more').html('Date Filters...');
        jQuery('.datefilters').show();
    }

    jQuery('.clear-filter').click(function() {
        jQuery('select[name=values]').val([]);
        jQuery('#'+grid+'-autofilter .textfilter').val('');
        submitAutofilterForm();
    });


    // jQuery('.dialogtest .autofilter-sort').click( doSort /*function() {  } */ );

    jQuery('.udf-equals').click(function() { showUserDefinedFilter('EQUALS'); } );
    jQuery('.udf-not-equals').click(function() { showUserDefinedFilter('NOT-EQUALS'); } );
    jQuery('.udf-starts-with').click(function() { showUserDefinedFilter('STARTS-WITH'); } );
    jQuery('.udf-ends-with').click(function() { showUserDefinedFilter('ENDS-WITH'); } );
    jQuery('.udf-contains').click(function() { showUserDefinedFilter('CONTAINS'); } );
    jQuery('.udf-not-contains').click(function() { showUserDefinedFilter('NOT-CONTAINS'); } );
    jQuery('.udf-between').click(function() { showUserDefinedFilter('BETWEEN'); } );
    jQuery('.udf-gt').click(function() { showUserDefinedFilter('GT'); } );
    jQuery('.udf-gte').click(function() { showUserDefinedFilter('GTE'); } );
    jQuery('.udf-lt').click(function() { showUserDefinedFilter('LT'); } );
    jQuery('.udf-lte').click(function() { showUserDefinedFilter('LTE'); } );
    jQuery('.udf-bool-yes').click(function() { $('.textfilter').val(1); submitAutofilterForm(); } );
    jQuery('.udf-bool-no').click(function() { $('.textfilter').val(0); submitAutofilterForm(); } );
    
    jQuery('.udf-date-before').click(function() { showUserDefinedFilter('LT'); } );
    jQuery('.udf-date-after').click(function() { showUserDefinedFilter('GT'); } );
    // Days
    jQuery('.udf-date-day-next').click(function() { setDateFilter('_DAY_NEXT_'); } );
    jQuery('.udf-date-day-this').click(function() { setDateFilter('_DAY_THIS_'); } );
    jQuery('.udf-date-day-last').click(function() { setDateFilter('_DAY_LAST_'); } );
    // Weeks
    jQuery('.udf-date-week-next').click(function() { setDateFilter('_WEEK_NEXT'); } );
    jQuery('.udf-date-week-this').click(function() { setDateFilter('_WEEK_THIS_'); } );
    jQuery('.udf-date-week-last').click(function() { setDateFilter('_WEEK_LAST_'); } );
    // Months
    jQuery('.udf-date-month-next').click(function() { setDateFilter('_MONTH_NEXT'); } );
    jQuery('.udf-date-month-this').click(function() { setDateFilter('_MONTH_THIS_'); } );
    jQuery('.udf-date-month-last').click(function() { setDateFilter('_MONTH_LAST_'); } );
    // Quarters
    jQuery('.udf-date-quarter-next').click(function() { setDateFilter('_QUARTER_NEXT'); } );
    jQuery('.udf-date-quarter-this').click(function() { setDateFilter('_QUARTER_THIS_'); } );
    jQuery('.udf-date-quarter-last').click(function() { setDateFilter('_QUARTER_LAST_'); } );
    // Years
    jQuery('.udf-date-year-next').click(function() { setDateFilter('_YEAR_NEXT'); } );
    jQuery('.udf-date-year-this').click(function() { setDateFilter('_YEAR_THIS_'); } );
    jQuery('.udf-date-year-last').click(function() { setDateFilter('_YEAR_LAST_'); } );
    
    jQuery('.udf-default').click(function() { showUserDefinedFilter('default'); } );

    $('.dialogtest').dialog({autoOpen: false,
        title: label,
        modal: true,
        //position:[posLeft,posTop],
        buttons:{ 
            'OK':function(){
                submitAutofilterForm();
            },
            'Cancel':function(){
                jQuery('.moreFilters').hide(400);
                moreFiltersShown = false;
                $(this).dialog('close');
            }
        }
    });
    $('.dialogtest').dialog('open');
    $('.dialogtest').keypress(function(e) {
        if (e.keyCode == $.ui.keyCode.ENTER) {
            e.preventDefault();
            //Close dialog and/or submit here...
            submitAutofilterForm();
        }
    });
    $('.textfilter').focus().select();
} // }}} 
function submitAutofilterForm() // {{{ 
{
    jQuery('.moreFilters').hide(400);
    moreFiltersShown = false;
    if($('select[name=values]').val()) {
        var selectedValues = $('select[name=values]').val();
        console.log(selectedValues[0]==['__ALL__']);
        // All selected?
        if(selectedValues[0]==['__ALL__']) {
            console.log('__ALL__');
            jQuery('select[name=values]').val([]);
            jQuery('#'+grid+'-autofilter .textfilter').val('');
        } else {
            result = [{operator: "IN", value:[]}];
            $("select[name=values] option:selected").each(function () {
                result[0].value[result[0].value.length] = $(this).val();
            });
            $('.textfilter').val(JSON.stringify(result));
        }
    }
    // Update (hidden) filter field
    $('input[name='+filterCellName+']').val($('.textfilter').val());
    // OLD:
    // $.fn.yiiGridView.update(grid,{data: $('#'+grid+'-autofilter').serialize()} );
    // Trigger Enter keypress on hidden field
    var press = jQuery.Event("keydown", {keyCode: 13 } );
    $('input[name='+filterCellName+']').trigger(press);

    $('i.autofilter').on('click',showAutofilterDialog);
 
    $('.dialogtest').dialog('close');
} // }}} 
function showUserDefinedFilter(mode) // {{{ 
{
    if(mode=='BETWEEN') {
        jQuery('select[name=operator1]').val('GTE');
        jQuery('input[name=andOr]').val('AND');
        jQuery('select[name=operator2]').val('LTE');
    } else if(mode=='default') {

    } else {
        jQuery('select[name=operator1]').val(mode);
    }
    $('.userDefinedFilter').dialog({autoOpen: false,
        title: label,
        modal: true,
        buttons:{ 
            'OK':function(){
                var result = [];
                var op1 = $('select[name=operator1]').val();
                var text1 = jQuery('input[name=udftext1]').val();
                result[result.length] = {operator: op1, value: text1};
                console.log(result);
                
                var op2 = $('select[name=operator2]').val();
                var text2 = jQuery('input[name=udftext2]').val();
                var andOr = $('input[name=andOr]:checked').val();
                console.log('andOr: '+andOr);
                
                if(text2!='') {
                    result[0]['join'] = andOr;
                    result[result.length] = {operator: op2, value: text2};
                }
                console.log(result);
                $('input.textfilter').val(JSON.stringify(result));
                // De-select multiple values dropdown
                $('select[name=values]').val([]);
                
                // reset fields
                $('select[name=operator1], select[name=operator2]').val('');
                jQuery('input[name=udftext1], input[name=udftext2]').val('');

                $(this).dialog('close');
                submitAutofilterForm();
            },
            'Cancel':function(){
                $(this).dialog('close');
            }
        }
    });
    $('.userDefinedFilter').dialog('open');
    $('input[name=udftext1]').focus().select();
} // }}}
function setDateFilter(filter) { // {{{ 
    $('.textfilter').val(JSON.stringify([{operator: filter,value:""}]));
    submitAutofilterForm();
} // }}} 
function doSort(order) { // {{{ 
    if(sorting) return;
    alert(order); 
    sorting = true;
    var sort = attribute;
    if(order=='desc') {
        sort += '.desc';
    }
    $('input[name='+filterCellName+']').parent().find('input[name='+modelName+'_sort]').remove();
    $('input[name='+filterCellName+']').parent().append('<input type="hidden" name="'+modelName+'_sort" value="'+sort+'" />'); 
    submitAutofilterForm(); 
    sorting=false;
} // }}} 
// {{{ *** BASE64 Functions ***
var keyStr = "ABCDEFGHIJKLMNOP" +
               "QRSTUVWXYZabcdef" +
               "ghijklmnopqrstuv" +
               "wxyz0123456789+/" +
               "=";
// {{{ encode64
function encode64(input) {
     input = escape(input);
     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
           enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
           enc4 = 64;
        }

        output = output +
           keyStr.charAt(enc1) +
           keyStr.charAt(enc2) +
           keyStr.charAt(enc3) +
           keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
     } while (i < input.length);

     return output;
} // }}} 
// {{{ decode64
function decode64(input) {
     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
     var base64test = /[^A-Za-z0-9\+\/\=]/g;
     if (base64test.exec(input)) {
        alert("There were invalid base64 characters in the input text.\n" +
              "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
              "Expect errors in decoding.");
     }
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

     do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
           output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
           output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

     } while (i < input.length);

     return unescape(output);
} // }}} 
// }}} End BASE&$ functions

// vim: set fdm=marker:
